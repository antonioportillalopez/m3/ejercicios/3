<!DOCTYPE html>
<!--
Muestra el día de la semana en la que estamos hoy
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $dia='';
        $dia=date('l');
        echo $dia.'<br>';
        switch ($dia) {
            case 'Monday':
                echo 'Hoy es lunes';
                break;
            case 'Tuesday':
                echo 'Hoy es martes';
                break;
            case 'Wednesday':
                echo 'Hoy es miércoles';
                break;
            case 'Thursday':
                echo 'Hoy es jueves';
                break;
            case 'Friday':
                echo 'Hoy es viernes';
                break;
            case 'Saturday':
                echo 'Hoy es sábado';
                break;
            case 'Sunday':
                echo 'Hoy es domingo';
                break;
            default :
                echo 'No es una fecha';
                break;
            
        }
        // put your code here
        ?>
    </body>
</html>

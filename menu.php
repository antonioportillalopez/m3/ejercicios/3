<?php
include 'contenido_menu.php';
/* pasamos array $ejercicios[0]
 *                             con dos arrays [archivo]=>'nombre_del_archivo_para_iniciar'
 *                                            [enunciado]=>'enunciado_descriptivo_del_ejercicio'
 */
?>
<!DOCTYPE html> <!-- esta etiqueta dice que es HTML5 -->
<html>
    <head>
        <title>TODO supply a title</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style> /*aspectos visuales */
            *{ /* selector */

                margin:0px;
                padding:0xp;
            }
            html,body{
                width: 100%; 
                height: 100%;
            }
        </style>

    </head>
    <body>
        <h2>MENÚ EJERCICIO 3</h2>
        <div>
            <ul> <!-- creación de menús-->
                <?php 
                foreach ($ejercicios as $valores) :
                                foreach ($valores as $archivo => $enunciado ):
                                        echo '<li><a href="'.$archivo.'">'. $enunciado.'</a></li>';
                                endforeach;
                endforeach;
                ?>
            </ul>
        </div>
    </body>
</html>

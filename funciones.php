<!DOCTYPE html>
<!--
FUNCIONES PERSONALIZADAS:
formulario(cantidad_de_cosas,'archivo_donde_lo_envia')   ejemplo: echo formulario(3,'recoge.php')

-->
<html>
    <head>
        <meta charset="UTF-8">
        <title>funciones personalizadas</title>
    </head>
    <body>
        <?php
        // ------------------- FORMULARIO ------------------------------------------------------------------------------------
        function formulario($n_pide,$archivo){
            $dibuja="";
            $dibuja='<form action="'.$archivo.'">';
            for($c=0;$c<$n_pide;$c++):
                $dibuja.= '<div><input name="dato'.($c+1).'"  required placeholder="dato '.($c+1).'"></div>';
            endfor;
            $dibuja.= '<input value="enviar" type="submit">';
            $dibuja.= '</form>';
            return $dibuja;
        }
        // ------------------- FIN DE FORMULARIO ------------------------------------------------------------------------------
        ?>
        
    </body>
</html>
